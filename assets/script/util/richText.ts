import { toHex } from "./string"

export function getTextWithColor(text: string, color: string, opacity = 255) {
  const hexCode = toHex(Math.floor(opacity));
  const colorCode = `${color}${hexCode}`;

  if (text.includes('</color>')) {
    return text.replace(/(?<=<color=)(.[^>]*)/, colorCode);
  } else {
    return `<color=${colorCode}>${text}</color>`;
  }
}