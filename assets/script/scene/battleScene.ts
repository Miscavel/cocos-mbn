
import { _decorator, Component, Node, director } from 'cc';
import { BattlefieldSoundtrack } from '../audio/soundtrack/battlefieldSoundtrack';
import { BATTLE_ZONE_EVENT } from '../enum/battleZone';
import { SCENE_KEY } from '../enum/scene';
import { TRANSITION_SCREEN_EVENT } from '../enum/transitionScreen';
import { BattleResult } from '../object/battleResult';
import { BattleZone } from '../object/battleZone';
import { TransitionScreen } from '../sprite/transitionScreen';
const { ccclass, property } = _decorator;

@ccclass('BattleScene')
export class BattleScene extends Component {
    @property(TransitionScreen)
    public readonly transitionScreen?: TransitionScreen;

    @property(BattlefieldSoundtrack)
    public readonly battlefieldSoundtrack?: BattlefieldSoundtrack;

    @property(BattleZone)
    public readonly battleZone?: BattleZone;

    @property(BattleResult)
    public readonly battleResult?: BattleResult;

    start() {
        this.transitionScreen?.fadeOut(1.5);
        this.battlefieldSoundtrack?.play();

        this.battleZone?.node.once(BATTLE_ZONE_EVENT.VICTORY, () => {
            this.battlefieldSoundtrack?.fadeOut(0.5);
            this.scheduleOnce(() => {
                this.battleResult?.victory();
            }, 0.5);
            this.scheduleOnce(() => {
                this.battleResult?.hide();
                this.restartBattleScene();
            }, 3);
        });

        this.battleZone?.node.once(BATTLE_ZONE_EVENT.GAME_OVER, () => {
            this.battlefieldSoundtrack?.fadeOut(0.5);
            this.scheduleOnce(() => {
                this.battleResult?.gameOver();
            }, 0.5);
            this.scheduleOnce(() => {
                this.battleResult?.hide();
                this.restartBattleScene();
            }, 5);
        });
    }

    private restartBattleScene() {
        this.transitionScreen?.fadeIn(1.5);
        this.transitionScreen?.node.once(TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE, () => {
            director.loadScene(SCENE_KEY.BATTLE);
        });
    }
}