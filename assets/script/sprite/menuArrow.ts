
import { tween, _decorator, Node, v3 } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('MenuArrow')
export class MenuArrow extends BaseSprite {
    constructor() {
        super('MenuArrow', ASSET_KEY.MENU_ARROW);
    }

    start() {
        this.playAnimation();
    }

    private playAnimation() {
        const { node } = this;
        const { x, y, z } = node.position;

        const rightX = x + 3;
        const leftX = x - 3;

        tween(node)
            .repeatForever(
                tween<Node>()
                .to(
                    0.1,
                    {
                        position: v3(rightX, y, z)
                    }
                )
                .to(
                    0.1,
                    {
                        position: v3(x, y, z)
                    }
                )
                .to(
                    0.1,
                    {
                        position: v3(leftX, y, z)
                    }
                )
                .to(
                    0.1,
                    {
                        position: v3(x, y, z)
                    }
                )
            )
            .start();
    }
}