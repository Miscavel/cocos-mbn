
import { _decorator, Component, Node, assetManager, AnimationClip } from 'cc';
import { ANIMATION_KEY } from '../enum/animation';
import { ASSET_KEY } from '../enum/asset';
import { generateAnimationClip } from '../util/animation';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('HitExplosion')
export class HitExplosion extends BaseSprite {
    private readonly hitExplosionAnimationKey = ANIMATION_KEY.HIT_EXPLOSION_ANIMATION;

    constructor() {
        super('HitExplosion', ASSET_KEY.HIT_EXPLOSION, 0);
    }

    onLoad() {
        super.onLoad();
        this.setupAnimation();
    }

    private setupAnimation() {
        const { hitExplosionAnimationKey } = this;

        const hitExplosionAnimationClip = this.getHitExplosionAnimationClip();

        if (hitExplosionAnimationClip) {
            this.animation?.createState(hitExplosionAnimationClip, hitExplosionAnimationKey);
        }
    }

    private getHitExplosionAnimationClip() {
        const { hitExplosionAnimationKey } = this;

        const animationAsset = assetManager.assets.get(hitExplosionAnimationKey);

        if (animationAsset) return animationAsset as AnimationClip;
    
        const animationClip = this.generateHitExplosionAnimationClip();

        if (animationClip) {
            assetManager.assets.add(hitExplosionAnimationKey, animationClip);
        }

        return animationClip;
    }

    public generateHitExplosionAnimationClip() {
        const { textureKey } = this;

        const animationClip = generateAnimationClip(
            assetManager, 
            textureKey, 
            [1, 2, 3, 4, 5, 6, 0], 
            32,
        );
        
        return animationClip;
    }

    public playHitExplosionAnimation() {
        this.animation?.play(this.hitExplosionAnimationKey);
    }
}
