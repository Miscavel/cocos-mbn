
import { _decorator } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('FullPanel')
export class FullPanel extends BaseSprite {
    constructor() {
        super('FullPanel', ASSET_KEY.FULL_PANEL);
    }
}