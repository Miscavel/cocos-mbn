import { _decorator, assetManager, AnimationClip } from 'cc';
import { ANIMATION_KEY } from '../enum/animation';
import { ASSET_KEY } from '../enum/asset';
import { generateAnimationClip } from '../util/animation';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('TitleBackground')
export class TitleBackground extends BaseSprite {
    private readonly animationKey = ANIMATION_KEY.TITLE_BACKGROUND_FLASH;

    constructor() {
        super('TitleBackground', ASSET_KEY.TITLE_BACKGROUND_IMAGE, 0);
    }

    onLoad() {
        super.onLoad();
        this.setupAnimation();
    }

    private setupAnimation() {
        const animationClip = this.generateAnimationClip();
        
        if (animationClip) {
            this.animation?.createState(animationClip, this.animationKey);
        }
    }

    private generateAnimationClip() {
        const { animationKey, textureKey } = this;

        const animationAsset = assetManager.assets.get(animationKey);

        if (animationAsset) return animationAsset as AnimationClip;

        const animationClip = generateAnimationClip(
            assetManager, 
            textureKey, 
            [0, 1, 2, 3, 4, 3, 2, 1, 0], 
            8,
            AnimationClip.WrapMode.Loop
        );

        if (animationClip) {
            assetManager.assets.add(animationKey, animationClip);
        }
        
        return animationClip;
    }

    public playAnimation() {
        this.animation?.play(this.animationKey);
    }
}
