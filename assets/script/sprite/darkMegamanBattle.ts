
import { _decorator, Component, Node, math } from 'cc';
import { MegamanBattle } from './megamanBattle';
const { ccclass, property } = _decorator;

@ccclass('DarkMegamanBattle')
export class DarkMegamanBattle extends MegamanBattle {
    private readonly darkColor = new math.Color(125, 125, 125, 255);
    
    start() {
        if(super.start) {
            super.start();
        }

        this.applyColor();
    }

    private applyColor() {
        const { darkColor, cannonBattle } = this;

        this.setColor(darkColor);
        cannonBattle?.setColor(darkColor);
    }
}
