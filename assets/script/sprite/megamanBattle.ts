
import { _decorator, AnimationClip, assetManager } from 'cc';
import { ANIMATION_KEY } from '../enum/animation';
import { ASSET_KEY } from '../enum/asset';
import { addHierarchyPathToCurves, generateAnimationClip, getActiveAnimationState, mergeAnimationClips } from '../util/animation';
import { BaseSprite } from './baseSprite';
import { CannonBattle } from './cannonBattle';
import { HitExplosion } from './hitExplosion';
const { ccclass, property } = _decorator;

@ccclass('MegamanBattle')
export class MegamanBattle extends BaseSprite {
    private readonly fadeInAnimationKey = ANIMATION_KEY.MEGAMAN_BATTLE_FADE_IN;
    
    private readonly shootCannonAnimationKey = ANIMATION_KEY.MEGAMAN_BATTLE_SHOOT_CANNON;

    private readonly hurtAnimationKey = ANIMATION_KEY.MEGAMAN_BATTLE_HURT;

    private readonly deletedAnimationKey = ANIMATION_KEY.MEGAMAN_BATTLE_DELETED;

    @property(CannonBattle)
    public readonly cannonBattle?: CannonBattle;

    @property(HitExplosion)
    public readonly hitExplosion?: HitExplosion;

    constructor() {
        super('MegamanBattle', ASSET_KEY.MEGAMAN_BATTLE, 0);
    }

    onLoad() {
        super.onLoad();
        this.setupAnimation();
    }

    private setupAnimation() {
        const { fadeInAnimationKey, shootCannonAnimationKey, hurtAnimationKey, deletedAnimationKey } = this;

        const fadeInAnimationClip = this.generateFadeInAnimationClip();
        
        if (fadeInAnimationClip) {
            this.animation?.createState(fadeInAnimationClip, fadeInAnimationKey);
        }

        const shootCannonAnimationClip = this.generateShootCannonAnimationClip();

        if (shootCannonAnimationClip) {
            this.animation?.createState(shootCannonAnimationClip, shootCannonAnimationKey);
        }

        const hurtAnimationClip = this.generateHurtAnimationClip();

        if (hurtAnimationClip) {
            this.animation?.createState(hurtAnimationClip, hurtAnimationKey);
        }

        const deletedAnimationClip = this.generateDeletedAnimationClip();

        if (deletedAnimationClip) {
            this.animation?.createState(deletedAnimationClip, deletedAnimationKey);
        }
    }

    private generateFadeInAnimationClip() {
        const { fadeInAnimationKey, textureKey } = this;

        const animationAsset = assetManager.assets.get(fadeInAnimationKey);

        if (animationAsset) return animationAsset as AnimationClip;

        const animationClip = generateAnimationClip(
            assetManager, 
            textureKey, 
            [24, 25, 26, 27], 
            32,
        );

        if (animationClip) {
            assetManager.assets.add(fadeInAnimationKey, animationClip);
        }
        
        return animationClip;
    }

    private generateShootCannonAnimationClip() {
        const { shootCannonAnimationKey, textureKey } = this;

        const animationAsset = assetManager.assets.get(shootCannonAnimationKey);

        if (animationAsset) return animationAsset as AnimationClip;

        const megamanAnimationClip = generateAnimationClip(
            assetManager,
            textureKey,
            [64, 65, 66, 67, 67, 67, 68, 69, 70, 71, 71, 71, 71, 71, 71, 56, 57, 58, 59],
            32,
        );

        const cannonAnimationClip = this.generateCannonShootAnimationClip();

        if (megamanAnimationClip && cannonAnimationClip) {
            const mergedAnimationClip = mergeAnimationClips([ megamanAnimationClip, cannonAnimationClip ]);
            assetManager.assets.add(shootCannonAnimationKey, mergedAnimationClip);

            return mergedAnimationClip;
        }

        return null;
    }

    private generateCannonShootAnimationClip() {
        const { cannonBattle } = this;

        if (!cannonBattle) return null;

        const animationClip = cannonBattle.generateCannonShootAnimationClip();
        
        if (animationClip) {
            animationClip.curves = addHierarchyPathToCurves(
                animationClip.curves, 
                cannonBattle.node.name
            );
        }

        return animationClip;
    }

    private generateHurtAnimationClip() {
        const { hurtAnimationKey, textureKey } = this;

        const animationAsset = assetManager.assets.get(hurtAnimationKey);

        if (animationAsset) return animationAsset as AnimationClip;

        const animationClip = generateAnimationClip(
            assetManager, 
            textureKey, 
            [8, 9, 10, 11, 12, 13, 14, 15], 
            32,
        );

        if (animationClip) {
            assetManager.assets.add(hurtAnimationKey, animationClip);
        }
        
        return animationClip;
    }

    private generateDeletedAnimationClip() {
        const { deletedAnimationKey, textureKey } = this;

        const animationAsset = assetManager.assets.get(deletedAnimationKey);

        if (animationAsset) return animationAsset as AnimationClip;

        const animationClip = generateAnimationClip(
            assetManager, 
            textureKey, 
            [8, 9, 10, 11, 12, 13, 13, 13], 
            32,
        );

        if (animationClip) {
            assetManager.assets.add(deletedAnimationKey, animationClip);
        }
        
        return animationClip;
    }

    private resetAnimation() {
        const { animation } = this;

        if (!animation) return;

        const activeAnimationState = getActiveAnimationState(animation);

        if (activeAnimationState) {
            const { duration } = activeAnimationState;
            activeAnimationState.setTime(duration);
            activeAnimationState.update(0);
        }
    }

    public playFadeInAnimation() {
        this.resetAnimation();
        this.animation?.play(this.fadeInAnimationKey);
    }

    public playShootCannonAnimation() {
        this.resetAnimation();
        this.animation?.play(this.shootCannonAnimationKey);
    }

    public playHurtAnimation() {
        this.resetAnimation();
        this.animation?.play(this.hurtAnimationKey);
        this.hitExplosion?.playHitExplosionAnimation();
    }

    public playDeletedAnimation() {
        this.resetAnimation();
        this.animation?.play(this.deletedAnimationKey);
        this.hitExplosion?.playHitExplosionAnimation();
    }
}
