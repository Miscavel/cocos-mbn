
import { _decorator, assetManager, AnimationClip } from 'cc';
import { ANIMATION_KEY } from '../enum/animation';
import { ASSET_KEY } from '../enum/asset';
import { generateAnimationClip } from '../util/animation';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('CannonBattle')
export class CannonBattle extends BaseSprite {
    private readonly cannonShootAnimationKey = ANIMATION_KEY.CANNON_SHOOT;

    constructor() {
        super('CannonBattle', ASSET_KEY.CANNON_BATTLE, 0);
    }

    private getCannonShootAnimationClip() {
        const { cannonShootAnimationKey } = this;

        const animationAsset = assetManager.assets.get(cannonShootAnimationKey);

        if (animationAsset) return animationAsset as AnimationClip;
    
        const animationClip = this.generateCannonShootAnimationClip();

        if (animationClip) {
            assetManager.assets.add(cannonShootAnimationKey, animationClip);
        }

        return animationClip;
    }

    public generateCannonShootAnimationClip() {
        const { textureKey } = this;

        const animationClip = generateAnimationClip(
            assetManager, 
            textureKey, 
            [0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 0, 0, 0], 
            32,
        );
        
        return animationClip;
    }
}