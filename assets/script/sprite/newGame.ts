
import { _decorator } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('NewGame')
export class NewGame extends BaseSprite {
    constructor() {
        super('NewGame', ASSET_KEY.TITLE_NEW_GAME);
    }
}