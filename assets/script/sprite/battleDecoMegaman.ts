import { _decorator, assetManager, AnimationClip } from 'cc';
import { ANIMATION_KEY } from '../enum/animation';
import { ASSET_KEY } from '../enum/asset';
import { generateAnimationClip } from '../util/animation';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('BattleDecoMegaman')
export class BattleDecoMegaman extends BaseSprite {
    private readonly animationKey = ANIMATION_KEY.BATTLE_DECO_MEGAMAN_ANIM;

    constructor() {
        super('BattleDecoMegaman', ASSET_KEY.BATTLE_DECO_MEGAMAN, 0);
    }

    start() {
        this.playAnimation();
    }

    onLoad() {
        super.onLoad();
        this.setupAnimation();
    }

    private setupAnimation() {
        const animationClip = this.generateAnimationClip();
        
        if (animationClip) {
            this.animation?.createState(animationClip, this.animationKey);
        }
    }

    private generateAnimationClip() {
        const { animationKey, textureKey } = this;

        const animationAsset = assetManager.assets.get(animationKey);

        if (animationAsset) return animationAsset as AnimationClip;

        const animationClip = generateAnimationClip(
            assetManager, 
            textureKey, 
            [0, 1, 2, 3, 4, 3, 2, 1, 0], 
            8,
            AnimationClip.WrapMode.Loop
        );

        if (animationClip) {
            assetManager.assets.add(animationKey, animationClip);
        }
        
        return animationClip;
    }

    public playAnimation() {
        this.animation?.play(this.animationKey);
    }
}
