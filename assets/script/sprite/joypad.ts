
import { UITransform, _decorator } from 'cc';
import { ASSET_KEY } from '../enum/asset';
import { BaseSprite } from './baseSprite';
const { ccclass, property } = _decorator;

@ccclass('Joypad')
export class Joypad extends BaseSprite {
    constructor() {
        super('Joypad', ASSET_KEY.JOYPAD);
    }
}