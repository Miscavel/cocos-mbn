
import { _decorator, Component, Node, UIOpacity } from 'cc';
import { MenuArrow } from '../sprite/menuArrow';
import { NewGame } from '../sprite/newGame';
import { TransitionScreen } from '../sprite/transitionScreen';
const { ccclass, property } = _decorator;

@ccclass('NewGameMenu')
export class NewGameMenu extends Component {
    @property(TransitionScreen)
    public readonly transitionScreen?: TransitionScreen;

    @property(NewGame)
    public readonly newGame?: NewGame;

    @property(MenuArrow)
    public readonly menuArrow?: MenuArrow;

    private uiOpacity?: UIOpacity | null;

    onLoad() {
        this.uiOpacity = this.getComponent(UIOpacity);
    }

    start() {
        this.setupElements();
        this.hide();
    }

    private setupElements() {
        this.transitionScreen?.setOpacity(150);
    }

    public setOpacity(opacity: number) {
        if (this.uiOpacity) {
            this.uiOpacity.opacity = opacity;
        }
    }

    public hide() {
        this.setOpacity(0);
    }

    public show() {
        this.setOpacity(255);
    }
}