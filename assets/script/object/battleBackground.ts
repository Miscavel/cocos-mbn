
import { _decorator, Component, Node, UITransform, instantiate, director } from 'cc';
import { BattleDecoMegaman } from '../sprite/battleDecoMegaman';
import { TransitionScreen } from '../sprite/transitionScreen';
const { ccclass, property } = _decorator;

@ccclass('BattleBackground')
export class BattleBackground extends Component {
    @property(TransitionScreen)
    public transitionScreen?: TransitionScreen;
    
    @property(BattleDecoMegaman)
    public battleDeco?: BattleDecoMegaman;

    private decoPool = new Array<BattleDecoMegaman>();

    start() {
        this.generateDecos();
    }

    private generateDecos() {
        const { battleDeco } = this;
        const { width, height } = this.transitionScreen?.getComponent(UITransform) || {};
        const { width: decoWidth, height: decoHeight } = battleDeco?.getComponent(UITransform) || {};

        if (!width || !height || !battleDeco || !decoWidth || !decoHeight) return;

        let zig = true;
        for (let row = decoHeight - height / 2; row < height / 2; row += decoHeight * 2) {
            const startingCol = (zig) ? -width / 2 : (- decoWidth * 4 - width) / 2;
            for (let col = startingCol; col < width / 2; col += decoWidth * 3) {
                const node = instantiate(battleDeco.node);
                node.setParent(this.node);
                node.setPosition(col, row);
                
                const deco = node.getComponent(BattleDecoMegaman);
                deco?.setOpacity(255);
                deco?.playAnimation();
                
                if (deco) this.decoPool.push(deco);
            }
            zig = !zig;
        }
    }
}
