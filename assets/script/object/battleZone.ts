
import { _decorator, Component, Node, v2 } from 'cc';
import { BATTLE_TILE_OWNER } from '../enum/battle';
import { BATTLE_ZONE_EVENT } from '../enum/battleZone';
import { UNIT_EVENT } from '../enum/unit';
import { BattleTile } from '../interface/battle';
import { Unit } from '../interface/unit';
import { BattleUnit } from '../unit/battleUnit';
const { ccclass, property } = _decorator;

@ccclass('BattleZone')
export class BattleZone extends Component {
    @property(BattleUnit)
    public readonly playerUnit?: BattleUnit;

    @property(BattleUnit)
    public readonly enemyUnit?: BattleUnit;
    
    private battleTiles: Array<Array<BattleTile>> = [
        [
            {
                index: v2(0, 0),
                position: v2(-399, -66),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(1, 0),
                position: v2(-239, -66),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(2, 0),
                position: v2(-79, -66),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(3, 0),
                position: v2(81, -66),
                owner: BATTLE_TILE_OWNER.ENEMY,
            },
            {
                index: v2(4, 0),
                position: v2(241, -66),
                owner: BATTLE_TILE_OWNER.ENEMY,
            },
            {
                index: v2(5, 0),
                position: v2(401, -66),
                owner: BATTLE_TILE_OWNER.ENEMY,
            }
        ],
        [
            {
                index: v2(0, 1),
                position: v2(-399, -162),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(1, 1),
                position: v2(-239, -162),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(2, 1),
                position: v2(-79, -162),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(3, 1),
                position: v2(81, -162),
                owner: BATTLE_TILE_OWNER.ENEMY,
            },
            {
                index: v2(4, 1),
                position: v2(241, -162),
                owner: BATTLE_TILE_OWNER.ENEMY,
            },
            {
                index: v2(5, 1),
                position: v2(401, -162),
                owner: BATTLE_TILE_OWNER.ENEMY,
            }
        ],
        [
            {
                index: v2(0, 2),
                position: v2(-399, -258),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(1, 2),
                position: v2(-239, -258),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(2, 2),
                position: v2(-79, -258),
                owner: BATTLE_TILE_OWNER.PLAYER,
            },
            {
                index: v2(3, 2),
                position: v2(81, -258),
                owner: BATTLE_TILE_OWNER.ENEMY,
            },
            {
                index: v2(4, 2),
                position: v2(241, -258),
                owner: BATTLE_TILE_OWNER.ENEMY,
            },
            {
                index: v2(5, 2),
                position: v2(401, -258),
                owner: BATTLE_TILE_OWNER.ENEMY,
            }
        ],
    ];

    private playerIndex = v2(1, 1);

    private enemyIndex = v2(4, 1);

    constructor() {
        super('BattleZone');
    }

    onLoad() {
        this.setTileUnit(this.playerIndex.x, this.playerIndex.y, this.playerUnit);
        this.setTileUnit(this.enemyIndex.x, this.enemyIndex.y, this.enemyUnit);

        this.playerUnit?.node.once(UNIT_EVENT.DELETED, () => {
            this.node.emit(BATTLE_ZONE_EVENT.GAME_OVER);
        });

        this.enemyUnit?.node.once(UNIT_EVENT.DELETED, () => {
            this.node.emit(BATTLE_ZONE_EVENT.VICTORY);
        });
    }

    private getTile(x: number, y: number) {
        const row = this.battleTiles[y];
        if (row) {
            return row[x];
        }
        return undefined;
    }

    private getTileWithOwner(x: number, y: number, owner: BATTLE_TILE_OWNER) {
        const tile = this.getTile(x, y);
        if (tile && tile.owner === owner) {
            return tile;
        }
        return undefined;
    }

    private getAvailableTilesWithOwner(owner: BATTLE_TILE_OWNER) {
        return this.battleTiles.reduce((res, row) => {
            const tiles = row.filter((col) => {
                return !col.unit && col.owner === owner;
            });

            res.push(...tiles);

            return res;
        }, new Array<BattleTile>());
    }

    public getRandomAvailableTileWithOwner(owner: BATTLE_TILE_OWNER) {
        const tiles = this.getAvailableTilesWithOwner(owner);

        return tiles[Math.floor(Math.random() * tiles.length)];
    }

    private setTileUnit(x: number, y: number, unit?: Unit) {
        const tile = this.getTile(x, y);
        if (tile) {
            tile.unit = unit;
        }
    }

    public movePlayer(x: number, y: number) {
        const { x: oldX, y: oldY } = this.playerIndex;
        const newX = oldX + x;
        const newY = oldY + y;
        const tile = this.getTileWithOwner(newX, newY, BATTLE_TILE_OWNER.PLAYER);
        if (tile) {
            this.setTileUnit(oldX, oldY, undefined);
            this.setTileUnit(newX, newY, this.playerUnit);
            this.playerIndex.set(newX, newY);
        }
    }

    public getPlayerTile() {
        const { x, y } = this.playerIndex;
        return this.getTile(x, y);
    }

    public moveEnemy(newX: number, newY: number) {
        const { x: oldX, y: oldY } = this.enemyIndex;
        const tile = this.getTileWithOwner(newX, newY, BATTLE_TILE_OWNER.ENEMY);
        if (tile) {
            this.setTileUnit(oldX, oldY, undefined);
            this.setTileUnit(newX, newY, this.enemyUnit);
            this.enemyIndex.set(newX, newY);
        }
    }

    public getEnemyTile() {
        const { x, y } = this.enemyIndex;
        return this.getTile(x, y);
    }

    public hitRow(row: number, damage: number, target: BATTLE_TILE_OWNER) {
        return this.battleTiles[row]?.reduce((res, tile) => {
            const { owner, unit } = tile;
            if (owner === target && unit && !unit.isInvincible) {
                unit.takeDamage(damage);
                return true;
            }
            return res;
        }, false);
    }

    public hitRowPlayer(damage: number) {
        const { y } = this.playerIndex;
        return this.hitRow(y, damage, BATTLE_TILE_OWNER.ENEMY);
    }

    public hitRowEnemy(damage: number) {
        const { y } = this.enemyIndex;
        return this.hitRow(y, damage, BATTLE_TILE_OWNER.PLAYER);
    }
}