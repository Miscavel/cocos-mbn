
import { _decorator, Component, Node, RichText } from 'cc';
import { BattleGameOverSoundtrack } from '../audio/soundtrack/battleGameOverSoundtrack';
import { BattleVictorySoundtrack } from '../audio/soundtrack/battleVictorySoundtrack';
const { ccclass, property } = _decorator;

@ccclass('BattleResult')
export class BattleResult extends Component {
    @property(RichText)
    public readonly youWinText?: RichText;

    @property(RichText)
    public readonly youLoseText?: RichText;

    @property(BattleVictorySoundtrack)
    public readonly battleVictorySoundtrack?: BattleVictorySoundtrack;

    @property(BattleGameOverSoundtrack)
    public readonly battleGameOverSoundtrack?: BattleGameOverSoundtrack;

    public victory() {
        const { node } = this.youWinText || {};
        if (node) {
            node.active = true;
        }
        this.battleVictorySoundtrack?.play();
    }

    public gameOver() {
        const { node } = this.youLoseText || {};
        if (node) {
            node.active = true;
        }
        this.battleGameOverSoundtrack?.play();
    }

    public hide() {
        if (this.youWinText?.node) {
            this.youWinText.node.active = false;
        }

        if (this.youLoseText?.node) {
            this.youLoseText.node.active = false;
        }
    }
}