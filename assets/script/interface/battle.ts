import { math } from "cc";
import { BATTLE_TILE_OWNER } from "../enum/battle";
import { Unit } from "./unit";

export interface BattleTile {
  index: math.Vec2,
  position: math.Vec2,
  owner: BATTLE_TILE_OWNER,
  unit?: Unit,
}