
import { _decorator, Component, Node } from 'cc';
import { TITLE_CONTROL_EVENT } from '../enum/titleControl';
const { ccclass, property } = _decorator;

@ccclass('TitleControl')
export class TitleControl extends Component {
    public registerTouchEvent() {
        this.node.on(Node.EventType.TOUCH_END, () => {
            this.node.emit(TITLE_CONTROL_EVENT.TOUCH_END);
        });
    }

    public unregisterTouchEvent() {
        this.node.off(Node.EventType.TOUCH_END);
    }
}
