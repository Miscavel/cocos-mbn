
import { _decorator, Component, systemEvent, SystemEvent } from 'cc';
import { CannonShootSfx } from '../audio/sfx/cannonShootSfx';
import { ExplosionImpactSfx } from '../audio/sfx/explosionImpactSfx';
import { BattleZone } from '../object/battleZone';
import { MegamanBattleUnit } from '../unit/megamanBattleUnit';
import { JoypadControl } from './joypadControl';
const { ccclass, property } = _decorator;

@ccclass('BattleControl')
export class BattleControl extends Component {
    @property(MegamanBattleUnit)
    public readonly megamanBattleUnit?: MegamanBattleUnit;

    @property(BattleZone)
    public readonly battleZone?: BattleZone;

    @property(JoypadControl)
    public readonly joypadControl?: JoypadControl;

    @property(CannonShootSfx)
    public readonly cannonShootSfx?: CannonShootSfx;

    @property(ExplosionImpactSfx)
    public readonly explosionImpactSfx?: ExplosionImpactSfx;

    private playerOffsetX = 35;

    private playerOffsetY = 75;

    start () {
        this.setPlayerPosition();
        this.registerControlEvent();
    }

    public registerControlEvent() {
        systemEvent.on(SystemEvent.EventType.KEY_UP, (event) => {
            switch(event.keyCode) {
                case 37: {
                    //left
                    this.movePlayer(-1, 0);
                    break;
                }

                case 38: {
                    //up
                    this.movePlayer(0, -1);
                    break;
                }

                case 39: {
                    //right
                    this.movePlayer(1, 0);
                    break;
                }

                case 40: {
                    //down
                    this.movePlayer(0, 1);
                    break;
                }

                case 90: {
                    //z
                    this.attack();
                    break;
                }

                default: {
                    break;
                }
            }
        });

        this.joypadControl?.node.on('move', (x: number, y: number) => {
            this.movePlayer(x, y);
        });

        this.joypadControl?.node.on('shoot', () => {
            this.attack();
        });
    }

    private movePlayer(x: number, y: number) {
        if (this.megamanBattleUnit?.move()) {
            this.battleZone?.movePlayer(x, y);
            this.setPlayerPosition();
        }
    }

    private setPlayerPosition() {
        const tile = this.battleZone?.getPlayerTile();

        if (!tile) return;
        
        const { x, y } = tile.position;
        this.megamanBattleUnit?.node.setPosition(x + this.playerOffsetX, y + this.playerOffsetY);
    }

    private attack() {
        if (this.megamanBattleUnit?.attack()) {
            this.cannonShootSfx?.play();
            this.scheduleOnce(() => {
                if (this.megamanBattleUnit?.isAttacking()) {
                    if (this.battleZone?.hitRowPlayer(40)) {
                        this.explosionImpactSfx?.play();
                    }
                }
            }, 0.2);
        }
    }
}
