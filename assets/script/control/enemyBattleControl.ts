
import { _decorator, Component, Node, macro, v2 } from 'cc';
import { CannonShootSfx } from '../audio/sfx/cannonShootSfx';
import { ExplosionImpactSfx } from '../audio/sfx/explosionImpactSfx';
import { BATTLE_TILE_OWNER } from '../enum/battle';
import { BattleZone } from '../object/battleZone';
import { BattleUnit } from '../unit/battleUnit';
const { ccclass, property } = _decorator;

@ccclass('EnemyBattleControl')
export class EnemyBattleControl extends Component {
    @property(BattleUnit)
    public readonly battleUnit?: BattleUnit;

    @property(BattleZone)
    public readonly battleZone?: BattleZone;

    @property(CannonShootSfx)
    public readonly cannonShootSfx?: CannonShootSfx;

    @property(ExplosionImpactSfx)
    public readonly explosionImpactSfx?: ExplosionImpactSfx;

    private unitOffsetX = -35;

    private unitOffsetY = 75;

    start () {
        this.setUnitPosition();
        this.registerControlEvent();
    }

    public registerControlEvent() {
        this.schedule(() => {
            this.moveToRandomTile();
        }, 1, macro.REPEAT_FOREVER)

        this.schedule(() => {
            if (Math.random() < 0.2) {
                this.moveToPlayerAlignedTile();
                this.attack();
            }
        }, 0.3, macro.REPEAT_FOREVER, 1);
    }

    private moveToRandomTile() {
        const tile = this.battleZone?.getRandomAvailableTileWithOwner(BATTLE_TILE_OWNER.ENEMY);
        if (tile) {
            const { x, y } = tile.index;
            this.moveUnit(x, y);
        }
    }

    private moveToPlayerAlignedTile() {
        const randomTile = this.battleZone?.getRandomAvailableTileWithOwner(BATTLE_TILE_OWNER.ENEMY);
        const playerTile = this.battleZone?.getPlayerTile();
        if (randomTile && playerTile) {
            this.moveUnit(randomTile.index.x, playerTile.index.y);
        }
    }

    private moveUnit(x: number, y: number) {
        if (this.battleUnit?.move()) {
            this.battleZone?.moveEnemy(x, y);
            this.setUnitPosition();
        }
    }

    private setUnitPosition() {
        const tile = this.battleZone?.getEnemyTile();

        if (!tile) return;
        
        const { x, y } = tile.position;
        this.battleUnit?.node.setPosition(x + this.unitOffsetX, y + this.unitOffsetY);
    }

    private attack() {
        if (this.battleUnit?.attack()) {
            this.cannonShootSfx?.play();
            this.scheduleOnce(() => {
                if (this.battleUnit?.isAttacking()) {
                    if (this.battleZone?.hitRowEnemy(40)) {
                        this.explosionImpactSfx?.play();
                    }
                }
            }, 0.2);
        }
    }
}