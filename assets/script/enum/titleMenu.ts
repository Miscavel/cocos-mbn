export enum TITLE_MENU_STATE {
  INIT = 'init',
  PRESS_START = 'press_start',
  NEW_GAME_MENU = 'new_game_menu',
  ENTERING_GAME = 'entering_game',
}