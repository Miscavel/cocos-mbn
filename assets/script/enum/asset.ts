export enum ASSET_EXTENSION {
  PNG = '.png',
}

export enum ASSET_TYPE {
  IMAGE = 'image',
  SPRITESHEET = 'spritesheet',
  AUDIO = 'audio',
}

export enum ASSET_KEY {
  DUMMY_1 = 'dummy_1',

  TITLE_BACKGROUND_IMAGE = 'title_background_image',
  TRANSITION_SCREEN = 'transition_screen',

  // title UI
  TITLE_PRESS_START = 'title_press_start',
  TITLE_NEW_GAME = 'title_new_game',

  // menu UI
  MENU_ARROW = 'menu_arrow',

  // game UI
  JOYPAD = 'joypad',

  // battle
  FULL_PANEL = 'full_panel',
  BATTLE_DECO_MEGAMAN = 'battle_deco_megaman',
  MEGAMAN_BATTLE = 'megaman_battle',
  CANNON_BATTLE = 'cannon_battle',
  HIT_EXPLOSION = 'hit_explosion',

  // soundtrack
  TITLE_BACKGROUND_SOUNDTRACK = 'title_background_soundtrack',
  BATTLEFIELD_SOUNTRACK = 'battlefield_soundtrack',
  BATTLE_VICTORY_SHORT = 'battle_victory_short',
  BATTLE_GAME_OVER = 'battle_game_over',

  // misc track
  SILENT_TRACK = 'silent_track',

  // menu sfx
  CUSTOM_CLICK_SFX = 'custom_click',
  NEW_GAME_CLICK_SFX = 'new_game_click_sfx',

  // battle sfx
  CANNON_SHOOT_SFX = 'cannon_shoot_sfx',
  EXPLOSION_IMPACT_SFX = 'explosion_impact_sfx',
}