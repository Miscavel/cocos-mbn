export enum ANIMATION_KEY {
  TITLE_BACKGROUND_FLASH = 'title_background_flash',
  BATTLE_DECO_MEGAMAN_ANIM = 'battle_deco_megaman_anim',

  MEGAMAN_BATTLE_FADE_IN = 'megaman_battle_fade_in',
  MEGAMAN_BATTLE_SHOOT_CANNON = 'megaman_battle_shoot_cannon',
  MEGAMAN_BATTLE_HURT = 'megaman_battle_hurt',
  MEGAMAN_BATTLE_DELETED = 'megaman_battle_deleted',

  CANNON_SHOOT = 'cannon_shoot',
  HIT_EXPLOSION_ANIMATION = 'hit_explosion_animation',
}