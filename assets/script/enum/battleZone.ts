export enum BATTLE_ZONE_EVENT {
  VICTORY = 'victory',
  GAME_OVER = 'game_over',
}