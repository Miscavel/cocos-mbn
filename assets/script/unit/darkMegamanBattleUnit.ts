import { _decorator, Component, Node, tween, UIOpacity } from 'cc';
import { DarkMegamanBattle } from '../sprite/darkMegamanBattle';
import { BattleUnit } from './battleUnit';
const { ccclass, property } = _decorator;

@ccclass('DarkMegamanBattleUnit')
export class DarkMegamanBattleUnit extends BattleUnit {
    @property(DarkMegamanBattle)
    public readonly darkMegamanBattle?: DarkMegamanBattle;

    public move() {
        if (super.move()) {
            this.darkMegamanBattle?.playFadeInAnimation();
            return true;
        }
        return false;
    }

    protected playBlinkInvincible() {
        const { invincibleDuration } = this;
        const uiOpacity = this.darkMegamanBattle?.getComponent(UIOpacity);

        if (!uiOpacity) return;

        tween(uiOpacity).to(
            invincibleDuration,
            {},
            {
                onUpdate() {
                    if (uiOpacity.opacity === 200) {
                        uiOpacity.opacity = 60;
                    } else {
                        uiOpacity.opacity = 200;
                    }
                },
                onComplete() {
                    uiOpacity.opacity = 255;
                }
            }
        ).start();
    }

    public attack() {
        if (super.attack()) {
            this.darkMegamanBattle?.playShootCannonAnimation();
            return true;
        }
        return false;
    }

    public takeDamage(damage: number) {
        if (super.takeDamage(damage)) {
            if (this.isDeleted()) {
                this.darkMegamanBattle?.playDeletedAnimation();
            } else {
                this.darkMegamanBattle?.playHurtAnimation();
            }
            return true;
        }
        return false;
    }
}
