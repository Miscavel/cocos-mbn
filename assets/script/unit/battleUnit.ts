
import { _decorator, Component, UIOpacity, tween } from 'cc';
import { UNIT_EVENT, UNIT_STATE } from '../enum/unit';
import { Unit } from '../interface/unit';
import { HpText } from '../text/hpText';
const { ccclass, property } = _decorator;

@ccclass('BattleUnit')
export abstract class BattleUnit extends Component implements Unit {
    readonly afterMoveDelay: number = 0;

    readonly afterAttackDelay: number = 0.6;

    readonly afterHurtDelay: number = 0.2;

    readonly invincibleDuration: number = 2;

    @property(Number)
    public maxHp = 100;

    @property(Number)
    public hp = 100;

    @property(HpText)
    public readonly hpText?: HpText;

    state = UNIT_STATE.READY;

    isInvincible = false;

    private uiOpacity?: UIOpacity | null;

    start() {
        this.hpText?.setHpText(this.hp);
    }

    onLoad() {
        this.uiOpacity = this.getComponent(UIOpacity);
    }

    public move() {
        if (!this.isReady()) return false;

        const { MOVING, READY } = UNIT_STATE;
        const { afterMoveDelay } = this;
        
        if (afterMoveDelay > 0) {
            this.state = MOVING;
            this.scheduleOnce(() => {
                if (this.isMoving()) {
                    this.state = READY;
                }
            }, afterMoveDelay);
        }

        return true;
    }

    public attack() {
        if (!this.isReady()) return false;

        const { ATTACKING, READY } = UNIT_STATE;
        const { afterAttackDelay } = this;

        this.state = ATTACKING;
        this.scheduleOnce(() => {
            if (this.isAttacking()) {
                this.state = READY;
            }
        }, afterAttackDelay);

        return true;
    }

    public getHurt() {
        const { HURTING , READY } = UNIT_STATE;
        const { afterHurtDelay } = this;

        this.state = HURTING;
        this.scheduleOnce(() => {
            if (this.isHurting()) {
                this.state = READY;
            }
        }, afterHurtDelay);

        return true;
    }

    public getDeleted() {
        const { DELETED } = UNIT_STATE;

        this.isInvincible = true;
        this.state = DELETED;
        this.disappear();

        this.node.emit(UNIT_EVENT.DELETED);
        
        return true;
    }

    public isReady() {
        return this.state === UNIT_STATE.READY;
    }

    public isMoving() {
        return this.state === UNIT_STATE.MOVING;
    }

    public isAttacking() {
        return this.state === UNIT_STATE.ATTACKING;
    }

    public isHurting() {
        return this.state === UNIT_STATE.HURTING;
    }

    public isDeleted() {
        return this.state === UNIT_STATE.DELETED;
    }

    protected playBlinkInvincible() {
        const { uiOpacity, invincibleDuration } = this;

        if (!uiOpacity) return;

        tween(uiOpacity).to(
            invincibleDuration,
            {},
            {
                onUpdate() {
                    if (uiOpacity.opacity === 200) {
                        uiOpacity.opacity = 60;
                    } else {
                        uiOpacity.opacity = 200;
                    }
                },
                onComplete() {
                    uiOpacity.opacity = 255;
                }
            }
        ).start();
    }

    protected disappear() {
        const { uiOpacity, node } = this;

        if (!uiOpacity) return;

        tween(uiOpacity).to(
            0.5,
            { opacity: 0 },
            {
                onComplete() {
                    node.active = false;
                }
            }
        ).start();
    }

    public turnInvincible() {
        if (this.isInvincible) return false;

        const { invincibleDuration } = this;

        this.isInvincible = true;
        this.getHurt();
        this.playBlinkInvincible();
        this.scheduleOnce(() => {
            this.isInvincible = false;
        }, invincibleDuration);

        return true;
    }

    public takeDamage(damage: number) {
        if (this.isInvincible) return false;

        if (this.hp <= damage) {
            this.getDeleted();
        } else {
            this.turnInvincible();
        }
        
        this.hp = Math.max(this.hp - damage, 0);
        this.hpText?.setHpText(this.hp);

        return true;
    }
}