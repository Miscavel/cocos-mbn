import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('BattleGameOverSoundtrack')
export class BattleGameOverSoundtrack extends BaseAudio {
    constructor() {
        super('BattlefieldSoundtrack', ASSET_KEY.BATTLE_GAME_OVER, false, 0.4);
    }
}
