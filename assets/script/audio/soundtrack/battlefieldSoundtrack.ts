import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('BattlefieldSoundtrack')
export class BattlefieldSoundtrack extends BaseAudio {
    constructor() {
        super('BattlefieldSoundtrack', ASSET_KEY.BATTLEFIELD_SOUNTRACK, true, 0.4);
    }
}
