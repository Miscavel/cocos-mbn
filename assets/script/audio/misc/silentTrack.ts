import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('SilentTrack')
export class SilentTrack extends BaseAudio {
    constructor() {
        super('SilentTrack', ASSET_KEY.SILENT_TRACK);
    }
}