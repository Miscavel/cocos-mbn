
import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('CannonShootSfx')
export class CannonShootSfx extends BaseAudio {
    constructor() {
        super('CannonShootSfx', ASSET_KEY.CANNON_SHOOT_SFX, false, 0.5);
    }
}
