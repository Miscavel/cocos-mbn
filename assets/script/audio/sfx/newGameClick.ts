
import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('NewGameClick')
export class NewGameClick extends BaseAudio {
    constructor() {
        super('NewGameClick', ASSET_KEY.NEW_GAME_CLICK_SFX);
    }
}
