
import { _decorator } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('CustomClick')
export class CustomClick extends BaseAudio {
    constructor() {
        super('CustomClick', ASSET_KEY.CUSTOM_CLICK_SFX);
    }
}