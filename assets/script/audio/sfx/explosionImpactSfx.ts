
import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../../enum/asset';
import { BaseAudio } from '../baseAudio';
const { ccclass, property } = _decorator;

@ccclass('ExplosionImpactSfx')
export class ExplosionImpactSfx extends BaseAudio {
    constructor() {
        super('CannonShootSfx', ASSET_KEY.EXPLOSION_IMPACT_SFX, false, 0.5);
    }
}
